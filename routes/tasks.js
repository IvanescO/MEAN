const express = require ("express");
const router = express.Router();
var mongojs = require("mongojs");
var db = mongojs("mongodb://ivan:ivan@ds145039.mlab.com:45039/sadbase", ["tasks"]);


//get all tasks
router.get("/tasks", (req,res, next)=>{
    db.tasks.find( (err, tasks)=>{
        if(err){
            res.send(err);
        }
        res.json(tasks);
    })
})

//get single task

router.get("/tasks/:id", (req,res, next)=>{
    db.tasks.findOne({_id: mongojs.ObjectId(req.params.id)}, (err, task)=>{
        if(err){
            res.send(err);
        }
        res.json(task);
    })
})

//save task
router.post("/task", (req ,res, next )=>{
    var task = req.body;
    if(!task.title){
            res.status(400);
            res.json(
                {
                    "error":"Bad data"

                }
            )
    } else {
    db.tasks.save(task, (err, task)=>{
            if(err){
                res.send(err);
            }
            console.log("RETuRn "+ task.title)
            res.json(task);
        });
    }
});

//delete task 

router.delete("/tasks/:id", (req,res, next)=>{
    db.tasks.remove({_id: mongojs.ObjectId(req.params.id)}, (err, task)=>{
        if(err){
            res.send(err);
        }
        res.json(task);
    })
});

// update task 

router.put("/tasks/:id", (req,res, next)=>{
    var task = req.body;
    var updTask = {};
    
    if(task.isDone){
        updTask.isDone = task.isDone;
    }

    if(task.title){
        updTask.title = task.title;
    }
    console.log(`UPDATE TASK ${updTask}`)
    if(!updTask){
        res.status(400);
        res.json({
            "err":"bad data"
        })
    } else {
    db.tasks.update({_id: mongojs.ObjectId(req.params.id)},updTask,{},(err, task)=>{
        if(err){
            res.send(err);
        }
        res.json(task);
    })

    }
    

})
module.exports = router;